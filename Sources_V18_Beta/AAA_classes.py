from math import *
import sys, pygame
import time
from pygame.locals import *


LARGEUR = 800
HAUTEUR = 600
MAX_SPEED = 1
SPEED_FACTOR = 2
DEMI_DIAGONALE = sqrt(LARGEUR*LARGEUR + HAUTEUR*HAUTEUR)/2 + 40
COMPTEUR = 0
black = 0,0,0
white = 255,255,255

def sign(n):
    if n > 0:
        return 1
    elif n == 0:
        return 0
    else:
        return -1

def near_int(x):
    if (x > 0):
        below = floor(x)
        if (x - below - 0.5 < 0):
            return below , x - below
        else:
            return below + 1 , x - (below + 1)
    else :
        below = floor(x)
        if ( x - below - 0.5 > 0):
            return below + 1 , x - (below + 1)
        else:
            return below , x - below

class ennemi:
    nb_ennemis = 0
    def __init__(self, rect, x, y):
        ennemi.nb_ennemis = ennemi.nb_ennemis + 1
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_speed = 0
        self.y_speed = 0
        self.x_speed_max = 1
        self.y_speed_max = 1
        self.x_goal = 0
        self.y_goal = 0
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))

    def move(self, x_speed, y_speed):
        self.rect.centerx = self.rect.centerx + x_speed
        self.rect.centery = self.rect.centery + y_speed
        if self.rect.centerx > LARGEUR:
            self.rect.centerx = 0
        if self.rect.centerx < 0:
            self.rect.centerx = LARGEUR
        if self.rect.centery > HAUTEUR:
            self.rect.centery = 0
        if self.rect.centery < 0:
            self.rect.centery = HAUTEUR
            
    def continue_move(self):
        if (self.fini == 0) :
            self.rect.centerx= self.rect.centerx + self.x_speed
            self.rect.centery= self.rect.centery + self.y_speed
            
            if ((self.rect.centerx ==  self.x_goal) and (self.rect.centery ==  self.y_goal)) :
                self.fini = 1
            if (self.rect.centerx ==  self.x_goal) :
                self.x_speed = 0
            if (self.rect.centery ==  self.y_goal) :
                self.y_speed = 0    
                
            if self.rect.centerx > LARGEUR:
                self.rect.centerx = 0
                self.x_goal = self.x_goal - LARGEUR
            if self.rect.centerx < 0:
                self.rect.centerx = LARGEUR
                self.x_goal = self.x_goal + LARGEUR
            if self.rect.centery > HAUTEUR:
                self.rect.centery = 0
                self.y_goal = self.y_goal - HAUTEUR
            if self.rect.centery < 0:
                self.rect.centery = HAUTEUR
                self.y_goal = self.y_goal + HAUTEUR
                
    def goto_xy(self, x, y,x_speed = MAX_SPEED,y_speed = MAX_SPEED): 
        if((x != self.rect.centerx) or (y != self.rect.centery)):
            self.fini = 0
            self.x_goal = x
            self.y_goal = y
            if (x < self.rect.centerx) :
                self.x_speed = -x_speed
            if (x > self.rect.centerx) :
                self.x_speed = x_speed
            if (y < self.rect.centery) :
                self.y_speed = -y_speed
            if (y > self.rect.centery) :
                self.y_speed = y_speed
            if (x == self.rect.centerx) :
                self.x_speed = 0
            if (y == self.rect.centery) :
                self.y_speed = 0

class ennemi_delire: # Effet inertiel de ouf !
    def __init__(self, rect, x, y, coef_inert, max_inert,vitesse = 1):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.x_cumul = 0
        self.y_cumul = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
        self.coef = coef_inert
        self.max_inert = max_inert
            
    def continue_move(self):      
        delta_x = self.x_goal - self.rect.centerx
        delta_y = self.y_goal - self.rect.centery
        
        if (abs(delta_x)> abs(delta_y)):
            if (abs(self.y_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.y_cumul)
                self.rect.centery = self.rect.centery + sign(rajout)*max(abs(rajout),self.max_inert)
                #self.y_cumul = self.y_cumul - self.coef*rajout
                
            rapport = abs(delta_y/delta_x)
            self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
            increment_y, reste = near_int(self.vit_max*rapport)
            self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
            self.y_cumul = self.y_cumul + reste
            
        else:
            if (abs(self.x_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.x_cumul)
                self.rect.centerx = self.rect.centerx + sign(rajout)*max(abs(rajout),self.max_inert)
                #self.x_cumul = self.x_cumul - self.coef*rajout
                
            rapport = abs(delta_x/delta_y)
            self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
            increment_x, reste = near_int(self.vit_max*rapport)
            self.rect.centerx = self.rect.centerx + increment_x*sign(delta_x)
            self.x_cumul = self.x_cumul + reste
            
                        
    def goto_xy(self, x, y): 
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery                

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + DEMI_DIAGONALE*sin(theta)*sign(delta_y)                

class ennemi_deluxe:
    def __init__(self, rect, x, y, vitesse = 1):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.x_cumul = 0
        self.y_cumul = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
            
    def continue_move(self):      
        delta_x = self.x_goal - self.rect.centerx
        delta_y = self.y_goal - self.rect.centery
        
        if (abs(delta_x)> abs(delta_y)):
            if (abs(self.y_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.y_cumul)
                self.rect.centery = self.rect.centery + rajout*sign(delta_y)
                self.y_cumul = self.y_cumul - rajout
                
            rapport = abs(delta_y/delta_x)
            self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
            increment_y, reste = near_int(self.vit_max*rapport)
            self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
            self.y_cumul = self.y_cumul + reste
            
        else:
            if (abs(self.x_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.x_cumul)
                self.rect.centerx = self.rect.centerx + rajout*sign(delta_x)
                self.x_cumul = self.x_cumul - rajout
                
            rapport = abs(delta_x/delta_y)
            self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
            increment_x, reste = near_int(self.vit_max*rapport)
            self.rect.centerx = self.rect.centerx + increment_x*sign(delta_x)
            self.x_cumul = self.x_cumul + reste
            
                        
    def goto_xy(self, x, y): 
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery                

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + 2*DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + 2*DEMI_DIAGONALE*sin(theta)*sign(delta_y)

class ennemi_sinus:
    def __init__(self, rect, x, y,only, periode,taille,vitesse = 1):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.x_cumul = 0
        self.y_cumul = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
        self.theta = 0
        self.periode = periode
        self.taille = taille
        self.only = only
            
    def continue_move(self):      
        delta_x = self.x_goal - self.rect.centerx
        delta_y = self.y_goal - self.rect.centery
        self.theta = self.theta + 1/self.periode
        
        if (abs(delta_x)> abs(delta_y)):
            if (abs(self.y_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.y_cumul)
                self.rect.centery = self.rect.centery + rajout*sign(delta_y)
                self.y_cumul = self.y_cumul - rajout
                
            rapport = abs(delta_y/delta_x)
            increment_x, inutile = near_int(self.vit_max + self.only*self.taille*sin(self.theta))
            self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
            increment_y, reste = near_int(self.vit_max*rapport + self.taille*cos(self.theta))
            self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
            self.y_cumul = self.y_cumul + reste
            
        else:
            if (abs(self.x_cumul) >= 1): #Prise en compte de la partie non-entiere negligee
                rajout, inutile = near_int(self.x_cumul)
                self.rect.centerx = self.rect.centerx + rajout*sign(delta_x) 
                self.x_cumul = self.x_cumul - rajout
                
            rapport = abs(delta_x/delta_y)
            increment_y, inutile = near_int(self.vit_max + self.taille*cos(self.theta))
            self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
            increment_x, reste = near_int(self.vit_max*rapport +self.only*self.taille*sin(self.theta))
            self.rect.centerx = self.rect.centerx + increment_x*sign(delta_x)
            self.x_cumul = self.x_cumul + reste
            
                        
    def goto_xy(self, x, y): 
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery                

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + 2*DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + 2*DEMI_DIAGONALE*sin(theta)*sign(delta_y)            
class tourelle:
    def __init__(self, rect, x, y):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))


class new_boulet:
    def __init__(self, rect, x, y, vitesse = 20):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.vit_max = vitesse
        self.fini = 0
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
            
    def continue_move(self):
        if (self.fini == 0) :       
            if ((self.rect.centerx > LARGEUR) or (self.rect.centerx < 0) or (self.rect.centery > HAUTEUR) or (self.rect.centery < 0)) :
                self.fini = 1
                
            delta_x = self.x_goal - self.rect.centerx
            delta_y = self.y_goal - self.rect.centery
            diff = abs(delta_y) - abs(delta_x)
            if (diff > 0):
                self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
            if (diff < 0):
                self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
            else :   
                self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
                self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
                            
    def goto_xy(self, x, y):
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + DEMI_DIAGONALE*sin(theta)*sign(delta_y)
            
class boulet_deluxe:
    def __init__(self, rect, x, y, vitesse = 20):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
            
    def continue_move(self):
        if (self.fini == 0) :       
            if ((self.rect.centerx > LARGEUR) or (self.rect.centerx < 0) or (self.rect.centery > HAUTEUR) or (self.rect.centery < 0)) :
                self.fini = 1
                return
                
            delta_x = self.x_goal - self.rect.centerx
            delta_y = self.y_goal - self.rect.centery
            
            if (delta_x == 0 and delta_y == 0):
                self.fini = 1
                return
            else :
                if (abs(delta_x)> abs(delta_y)):
                    rapport = abs(delta_y/delta_x)
                    self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
                    increment_y, inutile = near_int(self.vit_max*rapport)
                    self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
                    
                else:
                    rapport = abs(delta_x/delta_y)
                    self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
                    increment_x, inutile = near_int(self.vit_max*rapport)
                    self.rect.centerx = self.rect.centerx + increment_x*sign(delta_x)
                            
    def goto_xy(self, x, y): 
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery                

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + DEMI_DIAGONALE*sin(theta)*sign(delta_y)            

class kamehameha:
    def __init__(self, rect, x, y, vitesse = 7):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
            
    def continue_move(self):
        if (self.fini == 0) :       
            if ((self.rect.centerx > LARGEUR) or (self.rect.centerx < 0) or (self.rect.centery > HAUTEUR) or (self.rect.centery < 0)) :
                self.fini = 1
                
            delta_x = self.x_goal - self.rect.centerx
            delta_y = self.y_goal - self.rect.centery
            diff = abs(delta_y) - abs(delta_x)
            if (diff > 0):
                self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
            if (diff < 0):
                self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
            else :   
                self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
                self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)   
                      
    def goto_xy(self, x, y):
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + DEMI_DIAGONALE*sin(theta)*sign(delta_y)


class kamehameha_deluxe:
    def __init__(self, rect, x, y, vitesse = 20):
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.x_goal = 0
        self.y_goal = 0
        self.x_speed = 0
        self.y_speed = 0
        self.vit_max = vitesse
        self.fini = 1
        self.noir = pygame.Surface((self.rect.width,self.rect.height))
        self.noir.fill((0, 0, 0))
            
    def continue_move(self):
        if (self.fini == 0) :       
            if ((self.rect.centerx > LARGEUR) or (self.rect.centerx < 0) or (self.rect.centery > HAUTEUR) or (self.rect.centery < 0)) :
                self.fini = 1
                return
                
            delta_x = self.x_goal - self.rect.centerx
            delta_y = self.y_goal - self.rect.centery
            
            if (delta_x == 0 and delta_y == 0):
                self.fini = 1
                return
            else :
                if (abs(delta_x)> abs(delta_y)):
                    rapport = abs(delta_y/delta_x)
                    self.rect.centerx = self.rect.centerx + self.vit_max*sign(delta_x)
                    increment_y, inutile = near_int(self.vit_max*rapport)
                    self.rect.centery = self.rect.centery + increment_y*sign(delta_y)
                    
                else:
                    rapport = abs(delta_x/delta_y)
                    self.rect.centery = self.rect.centery + self.vit_max*sign(delta_y)
                    increment_x, inutile = near_int(self.vit_max*rapport)
                    self.rect.centerx = self.rect.centerx + increment_x*sign(delta_x)
                            
    def goto_xy(self, x, y): 
        if ( (x - self.rect.centerx)!=0 and (y - self.rect.centery)!=0):
            self.fini = 0
            delta_x = x - self.rect.centerx
            delta_y = y - self.rect.centery                

            theta = atan(abs(delta_y)/abs(delta_x))
            self.x_goal = LARGEUR/2 + DEMI_DIAGONALE*cos(theta)*sign(delta_x)
            self.y_goal = HAUTEUR/2 + DEMI_DIAGONALE*sin(theta)*sign(delta_y)

class bombe:
    def __init__(self, screen,rect, x, y, portee = 20): 
        self.screen = screen
        self.rect = rect
        self.rect.centerx = x
        self.rect.centery = y
        self.x_init = x
        self.y_init = y
        self.portee = portee
        self.boom = 10
        self.compteur = 0
        self.fini = 0
        self.compteur = 0

    def next_detonation(self):
        self.compteur = self.compteur + 1
        if((self.fini != 1) and (self.compteur%7 == 0)):
            pygame.draw.rect(self.screen, black, self.rect, 3)
            self.rect.inflate_ip(self.rect.width + self.boom, self.rect.height + self.boom)
            pygame.draw.rect(self.screen, white, self.rect, 3)
            self.compteur = self.compteur + 1
            if (self.compteur >= self.portee):
                self.fini = 1
