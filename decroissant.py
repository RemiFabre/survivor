def max_liste(liste) :
    taille = len(liste)
    max = -1
    i_elu = -1
    for i in range(taille):
        if (max < liste[i]):
            max = liste[i]
            i_elu = i
    return max,i_elu            

def copie_liste(liste):
    taille = len(liste)
    new_liste = []
    for i in range(taille):
        new_liste.append(liste[i])
    return new_liste    

def ordre_decroissant(liste):
    new_liste = []
    copie = copie_liste(liste)
    for i in range(len(liste)):
        max,i_elu = max_liste(copie)
        copie.pop(i_elu)
        new_liste.append(max)
    return new_liste


    
##liste.append(2)
##liste.append(0)
##liste.append(14)
##liste.append(6)
##liste.append(5)
##liste.append(1)
##liste.append(3)
##liste.append(75)
##liste.append(31)
##liste.append(3)
##
####print(liste)
####print(liste[1:])
##
##print("le max vaut: ", max_liste(liste))
##
##for i in liste:
##    print(i)
##
##copie = copie_liste(liste)  
##
##for i in copie:
##    print(i)
##
##liste2 = ordre_decroissant(copie)
##
##print("   ")
##
##for i in liste2:
##    print(i)



    
