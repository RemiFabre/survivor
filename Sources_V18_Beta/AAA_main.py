from math import *
import sys, pygame
import time
import random
from pygame.locals import *
from Sources_V18_Beta.AAA_classes import *

LARGEUR = 800
HAUTEUR = 600
MAX_SPEED = 1
SPEED_FACTOR = 2
DEMI_DIAGONALE = sqrt(LARGEUR*LARGEUR + HAUTEUR*HAUTEUR)/2 + 40
COMPTEUR = 0
black = 0,0,0
white = 255,255,255

pygame.mixer.init()
pygame.mixer.init()
pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.init()



def ennemis_cercle(nombre, phi = 0) : #Non-centre
    theta = (2*pi/nombre)
    result = []
    prec = 0
    
    for i in range(nombre):
        x = floor(HAUTEUR*cos(theta*i + phi) + HAUTEUR/2)
        y = floor(LARGEUR*sin(theta*i + phi) + LARGEUR/2)
        result.append(x)
        result.append(y)
    return result

def ennemis_3coins():
    result = []
    
    x = 0
    y = 0
    result.append(x)
    result.append(y)

    x = LARGEUR
    y = 0
    result.append(x)
    result.append(y)

    x = LARGEUR
    y = HAUTEUR
    result.append(x)
    result.append(y)

    return result

def ennemis_3coins_bis():
    result = []
    
    x = 0
    y = 0
    result.append(x)
    result.append(y)

    x = LARGEUR
    y = HAUTEUR
    result.append(x)
    result.append(y)

    x = 0
    y = HAUTEUR
    result.append(x)
    result.append(y)
    return result


def ennemis_random(nombre) :
    result = []
    for i in range(nombre):
        cote = random.randint(0, 3)
        if (cote == 0):
            x = random.randint(0, LARGEUR)
            y = 0 - 20
            result.append(x)
            result.append(y)
        elif (cote == 1):
            x = random.randint(0, HAUTEUR)
            y = LARGEUR + 20
            result.append(x)
            result.append(y)
        elif (cote == 2):
            x = 0 - 20
            y = random.randint(0, LARGEUR)
            result.append(x)
            result.append(y)
        elif (cote == 3):
            x = HAUTEUR + 20
            y = random.randint(0, HAUTEUR)
            result.append(x)
            result.append(y)
        else:
            print("Fail avec le random")
    return result    
    
def new_ennemi(liste_ennemis, dora_rect,x,y):
    o = ennemi(dora_rect, x,y)    
    liste_ennemis.append(o)

def new_ennemi_quart(liste_ennemis, dora_rect):
    global COMPTEUR
    
    if((COMPTEUR % 4)==0):
        o = ennemi(dora_rect, 0,0)
    if((COMPTEUR % 4)==1):
        o = ennemi(dora_rect, LARGEUR,0)
    if((COMPTEUR % 4)==2):
        o = ennemi(dora_rect, 0,HAUTEUR)
    if((COMPTEUR % 4)==3):
        o = ennemi(dora_rect, LARGEUR,HAUTEUR)    
    liste_ennemis.append(o)
    COMPTEUR = COMPTEUR + 1

def rotation_xy(img,x,y):
    new_x = x - LARGEUR/2
    new_y = y - HAUTEUR/2
    if(new_x == 0):
        angle = pi/2
    else:    
        if (new_x > 0):
            angle = -atan(new_y/new_x)
        elif (new_y > 0):
            angle = -pi + atan(abs(new_y)/abs(new_x))
        else :
            angle = -atan(abs(new_y)/abs(new_x)) + pi
    new_img = pygame.transform.rotate(img, angle*180/pi)
    return new_img

def main(screen, lvl, enable_kamehameha = 0, nb_kamehameha_max = 3, enable_slow_down = 0,nb_slow_down_max = 1,length_slow_down = 0, enable_bomb = 0, nb_bomb_max = 1):
     
    #Chargement des images############################################################
    tourelle_image = pygame.image.load('tourelle.jpg').convert()
    tourelle_rect = tourelle_image.get_rect()
    dora_image = pygame.image.load('ennemi_1.jpg').convert()
    dora_rect = dora_image.get_rect()
    tourelle_rect = tourelle_image.get_rect()
    boulet_image = pygame.image.load('balle_1.jpg').convert()
    boulet_rect = boulet_image.get_rect()
    kamehameha_image = pygame.image.load('balle_2.jpg').convert()
    kamehameha_rect = kamehameha_image.get_rect()
    game_over_image = pygame.image.load('game_over_3.jpg').convert()
    game_over_rect = game_over_image.get_rect()
    victoire_image = pygame.image.load('victoire.jpg').convert()
    victoire_rect = victoire_image.get_rect()
    congratulations_image = pygame.image.load('congratulations.jpg').convert()
    congratulations_rect = congratulations_image.get_rect()
    
    #Chargement des sons#############################
    
    dora_sound = pygame.mixer.Sound('dora_mort1.wav')
    dora2_sound = pygame.mixer.Sound('dora_mort2.wav')
    dora3_sound = pygame.mixer.Sound('dora_plotch.wav')
    game_over_sound = pygame.mixer.Sound('game_over.wav')
    victoire_sound = pygame.mixer.Sound('victoire.wav')
    fond_sound = pygame.mixer.Sound('i_can_walk_on_water_remix.wav')
    kamehameha_sound = pygame.mixer.Sound('kamehameha_final2.wav')
    reward_sound = pygame.mixer.Sound('reward.wav')
    Finish_Him_sound = pygame.mixer.Sound('Finish_Him.wav')
    fond_sound.play()
##    pygame.mixer.music.load('i_can_walk_on_water_remix.mp3')
##    pygame.mixer.music.play()

    # Fourberie###################################
    petit_rect = boulet_image.get_rect()
    petit_rect.centerx = LARGEUR/2
    petit_rect.centery = HAUTEUR/2
    
    
    o_tourelle = tourelle(tourelle_rect,LARGEUR/2,HAUTEUR/2)
    screen.blit(tourelle_image, o_tourelle.rect)


    # Initialisation des variables ###################################
    
    liste_ennemis = []
    
    
    j = 0
    etat_j = 0
    clock = pygame.time.Clock()

    nb_ennemis = 0
    fin_boulet = 1
    fin_kamehameha = 1
    premier_boulet = 0
    premier_kamehameha = 0
    premiere_bombe = 0
    collision_kamehameha = 0
    collision_bombe = 0
    suppression_kamehameha = 0
    death = 0
    nb_detruits = 0
    nb_horde = 4
    etat_son = 0
    nb_kamehameha = 0
    nb_bomb = 0
    compteur_tick = 0
    slow_down = 0
    nb_slow_down = 0
    angle = 0
    money = 0
    compteur_lvl5 = 0
    
    #pygame.time.delay(1000) #"Temps de chargement"
    screen.fill(black)
    screen.blit(tourelle_image, o_tourelle.rect)
    pygame.display.update()
    
    #pygame.time.set_timer(USEREVENT+1, 1000)
    # Frequence d'apparition, nombre a tuer et reward #################################################
    temps = pygame.time.get_ticks()
    if (lvl == 0):
        temps_apparition = 3000
        nb_detruits_max = 100
    if (lvl == 1):
        temps_apparition = 2000
        nb_detruits_max = 30
        money = 150
    if (lvl == 2):
        temps_apparition = 1800
        nb_detruits_max = 40
        money = 200
    if (lvl == 3):
        temps_apparition = 5000
        nb_detruits_max = 45
        money = 250
    if (lvl == 4):
        temps_apparition = 1400
        nb_detruits_max = 45
        money = 300
    if (lvl == 5):
        temps_apparition = 800
        nb_detruits_max = 90
        money = 300    
    if (lvl == 6):
        temps_apparition = 1500
        nb_detruits_max = 40
        money = 450    
    if (lvl == 7):
        temps_apparition = 1000
        nb_detruits_max = 61
    if (lvl == 8):
        temps_apparition = 3000
        nb_detruits_max = 75
    if (lvl == 9):
        temps_apparition = 1000
        nb_detruits_max = 40

    temps = temps - temps_apparition + 1000    
        
    #Creation initiale d'ennemis :
##    nb_ennemis = 3 
##    coordonnees = ennemis_cercle(nb_ennemis)
##    for i in range(len(coordonnees)//2):
##        o = ennemi(dora_image.get_rect(), coordonnees[2*i], coordonnees[2*i +1])
##        liste_ennemis.append(o)
    
    while 1:
        if (nb_detruits > nb_detruits_max and (len(liste_ennemis)==0 or delta_t>10000)):
            if(lvl<9):
                screen.fill(black)
##                victoire_rect.centerx = LARGEUR/2
##                victoire_rect.centery = HAUTEUR/2
##                screen.blit(victoire_image, victoire_rect)
##                pygame.display.update()
                fond_sound.stop()
                victoire_sound.play()
                return money
            else:
                screen.fill(white)
                screen.blit(congratulations_image, congratulations_rect)
                pygame.display.update()
                fond_sound.stop()
                reward_sound.play()
            pygame.time.wait(600)    
            while(1) :
                for event in pygame.event.get():
                    if event.type == QUIT:
                        victoire_sound.stop()
                        return money
                    if event.type == KEYDOWN:
                        victoire_sound.stop()
                        return money
        elif (death == 1):
            screen.fill(black)
            game_over_rect.centerx = LARGEUR/2
            game_over_rect.centery = HAUTEUR/2
            screen.blit(game_over_image, game_over_rect)
            pygame.display.update()
            fond_sound.stop()
            game_over_sound.play()
            pygame.time.wait(600)
            while(1) :
                for event in pygame.event.get():
                    if event.type == QUIT:
                        game_over_sound.stop()
                        return -1
                    if event.type == KEYDOWN:
                        game_over_sound.stop()
                        return -1
        else :
            
            clock.tick(60)#60
                        
            if (slow_down == 1):
                clock.tick(20)
                compteur_tick = compteur_tick + 1
                if(compteur_tick == 50):
                    compteur_tick = 0
                    slow_down = 0
            if (slow_down == 2):
                clock.tick(20)
                compteur_tick = compteur_tick + 1
                if(compteur_tick == 90):
                    compteur_tick = 0
                    slow_down = 0        
                    
            delta_t = pygame.time.get_ticks() - temps
            
            if(slow_down != 0):
                temps_apparition_effectif = 3*temps_apparition    
            else:
                temps_apparition_effectif = temps_apparition
                
            #Gestion_creation_ennemis#############################################################
            
            if(delta_t > temps_apparition_effectif and nb_detruits < nb_detruits_max):
                temps = temps + temps_apparition_effectif
                if (lvl == 0):
                    nb_horde = 1
                if (lvl == 1):
                    if (nb_horde == 2):
                        nb_horde = 3
                    else :
                        nb_horde = 2
                    angle = angle + pi/8    
                    coordonnees = ennemis_cercle(nb_horde, angle)
                if (lvl == 2):
                    nb_horde = 3    
                    coordonnees = ennemis_random(nb_horde)
                if (lvl == 3):
                    nb_horde = 11
                    angle = angle + pi/8    
                    coordonnees = ennemis_cercle(nb_horde, angle)
                if (lvl == 4): # Ca saute de partout !
                    if (nb_horde == 1):
                        nb_horde = 5
                    if (nb_horde == 5):
                        nb_horde = 2
                    if (nb_horde == 2):
                        nb_horde = 4
                    if (nb_horde == 4) :
                        nb_horde = 1
                    else :
                        nb_horde = 1
                    angle = angle + pi/5    
                    coordonnees = ennemis_cercle(nb_horde, angle)
                if (lvl == 5):
                    if (compteur_lvl5<=20):
                        coordonnees = ennemis_3coins()
                    if (compteur_lvl5>=11):
                        coordonnees = ennemis_3coins_bis()
                    compteur_lvl5 = compteur_lvl5 + 1
                    if (compteur_lvl5 >40):
                        compteur_lvl5 = 0
                if (lvl == 6): # Petits diablotins
                    if (nb_horde == 2):
                        nb_horde = 3
                    else :
                        nb_horde = 2
                    angle = angle + pi/8    
                    coordonnees = ennemis_cercle(nb_horde, angle)    
                if (lvl == 7):
                    if (nb_horde == 3):
                        nb_horde = 4
                    elif (nb_horde == 4):
                        nb_horde = 5
                    else :
                        nb_horde = 3
                    coordonnees = ennemis_cercle(nb_horde)    
                if (lvl == 8):
                    if (nb_horde == 10):
                        nb_horde = 12
                    else:
                        nb_horde = 10
                    coordonnees = ennemis_cercle(nb_horde)    
                if (lvl == 9):
                    if (nb_horde == 1):
                        nb_horde = 2
                    elif (nb_horde == 2):
                        nb_horde = 3
                    elif (nb_horde == 3):
                        nb_horde = 4    
                    else:
                        nb_horde = 1
                    coordonnees = ennemis_cercle(nb_horde)    

                if (lvl == 6) :
                    for k in range(len(coordonnees)//2):
                        o = ennemi_delire(dora_image.get_rect(), coordonnees[2*k], coordonnees[2*k +1], 0.01,2)
                        liste_ennemis.append(o)
                if (lvl == 4) :
                    for k in range(len(coordonnees)//2):
                        o = ennemi_sinus(dora_image.get_rect(), coordonnees[2*k], coordonnees[2*k +1],1, 10,7)
                        liste_ennemis.append(o)
                if (lvl == 5) :
                    for k in range(len(coordonnees)//2):
                        o = ennemi_sinus(dora_image.get_rect(), coordonnees[2*k], coordonnees[2*k +1],0, 10,7)
                        liste_ennemis.append(o)        
                else:
                    for k in range(len(coordonnees)//2):
                        o = ennemi_deluxe(dora_image.get_rect(), coordonnees[2*k], coordonnees[2*k +1])
                        liste_ennemis.append(o)
                
            
            #Interactions avec l'utilisateurs ########################################################
            j = j + 1 #Nombre de tours
            for event in pygame.event.get():
                if event.type == QUIT:
                    fond_sound.stop()
                    return -1
                if (fin_boulet == 1):
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        o_boulet = boulet_deluxe(boulet_rect,LARGEUR/2,HAUTEUR/2)
                        x_mouse,y_mouse = pygame.mouse.get_pos()    
                        o_boulet.goto_xy(x_mouse,y_mouse)
                        premier_boulet = 1
                        
                if(fin_kamehameha == 1):
                    if event.type == pygame.KEYDOWN :
                        if (event.key == K_c and enable_kamehameha == 1):
                            if (nb_kamehameha < nb_kamehameha_max) :
                                o_kamehameha = kamehameha_deluxe(kamehameha_rect, LARGEUR/2,HAUTEUR/2)
                                x_mouse,y_mouse = pygame.mouse.get_pos()    
                                o_kamehameha.goto_xy(x_mouse,y_mouse)
                                if (premier_kamehameha == 0):
                                    nb_detruits_max = nb_detruits_max + 15
                                premier_kamehameha = 1
                                nb_kamehameha = nb_kamehameha + 1
                                kamehameha_sound.play()
                                suppression_kamehameha = 0
                            else :
                                Finish_Him_sound.play()
                        elif (event.key == K_b and enable_bomb == 1):
                            if(nb_bomb < nb_bomb_max and enable_bomb == 1):
                                o_bombe = bombe(screen,tourelle_image.get_rect(),LARGEUR/2,HAUTEUR/2) #tourelle_rect
                                premiere_bombe = 1
                                nb_bomb = nb_bomb + 1
                            else :
                                Finish_Him_sound.play()    
                        elif event.key == K_SPACE: #Gestion du ralentissement
                            if(nb_slow_down < nb_slow_down_max and enable_slow_down == 1):
                                nb_slow_down = nb_slow_down + 1
                                if (length_slow_down == 0):
                                    slow_down = 1
                                else:
                                    slow_down = 2
                            else :
                                Finish_Him_sound.play()        

            # Deplacement du boulet ##################################################################################            
            if(premier_boulet == 1): #Il faut deja en avoir tire un
                screen.blit(o_boulet.noir, o_boulet.rect) # On efface l'ancien rectangle
                if (o_boulet.fini == 0):
                    o_boulet.continue_move()
                    screen.blit(boulet_image, o_boulet.rect)
                if (o_boulet.fini == 1):
                    screen.blit(o_boulet.noir, o_boulet.rect)

                fin_boulet = o_boulet.fini

            # Deplacement du kamehameha ##################################################################################            
            if(premier_kamehameha == 1): #Il faut deja en avoir tire un
                if (o_kamehameha.fini == 0):
                    o_kamehameha.continue_move()
                    screen.blit(kamehameha_image, o_kamehameha.rect)
                if (o_kamehameha.fini == 1):
                    screen.blit(o_kamehameha.noir, o_kamehameha.rect)

                fin_kamehameha = o_kamehameha.fini
                if(fin_kamehameha == 1):
                    if(suppression_kamehameha == 0):
                        o_kamehameha.rect.centerx = LARGEUR/2
                        o_kamehameha.rect.centery = HAUTEUR/2
                        screen.fill(black)
                    suppression_kamehameha = 1
            #Detonation Bombe #########################################################################################
            if(premiere_bombe == 1):
                if(o_bombe.fini == 0):
                    o_bombe.next_detonation()
                if(o_bombe.fini == 1):
                    o_bombe.compteur = o_bombe.compteur + 1
                    if (o_bombe.compteur%11 == 10):
                        pygame.draw.rect(screen, black, o_bombe.rect, 3)
                        o_bombe.rect = petit_rect

            # Deplacement des ennemis ##################################################################################     
            i = 0
            for o in liste_ennemis: 
                screen.blit(o.noir, o.rect) # On efface l'ancien rectangle
                if(o.fini == 1):
                    if(lvl == 9):
                        o.goto_xy(LARGEUR/2,HAUTEUR/2)
                        o.vit_max = 4 #Traitrise absolue
                    if(lvl == 6):
                        o.goto_xy(LARGEUR/2,HAUTEUR/2)
                        o.vit_max = 1 #Traitrise absolue
                    if(lvl == 4):
                        o.goto_xy(LARGEUR/2,HAUTEUR/2)
                        o.vit_max = 1 #Traitrise absolue    
                    else :
                        o.goto_xy(LARGEUR/2,HAUTEUR/2)
                if(o.fini == 0):
                    o.continue_move()
                i = i + 1
                
                screen.blit(dora_image, o.rect)
            ind = 0
            
            #Gestion collision ####################################
            collision = 0    
            sortie = 0
            compteur = 0
            for jind in liste_ennemis:
                if (sortie == 1) :
                    continue
                if (premier_boulet == 1):
                    collision = o_boulet.rect.colliderect(jind.rect)
                if (premier_kamehameha == 1):
                    collision_kamehameha = o_kamehameha.rect.colliderect(jind.rect)
                if (premiere_bombe == 1):
                    collision_bombe = o_bombe.rect.colliderect(jind.rect)
                death = o_tourelle.rect.colliderect(jind.rect)
                if (death == 1):
                    break
                if ( collision == 1):
                    sortie = 1 #Une seule collision a la fois
                    #Suppression de l'ennemi :
                    screen.blit(jind.noir, jind.rect)
                    liste_ennemis.pop(compteur)
                    if (etat_son == 1):
                        dora_sound.play()
                        etat_son = 2
                    elif (etat_son == 2):
                        dora2_sound.play()
                        etat_son = 0
                    elif (etat_son == 0):
                        dora3_sound.play()
                        etat_son = 1    
                    #suppression_ennemi(jind,compteur)
                    #Suppression du boulet :
                    screen.blit(o_boulet.noir, o_boulet.rect)
                    o_boulet.rect.centerx = LARGEUR/2 #On le vire le temps qu'un autre soit tire
                    o_boulet.rect.centery = HAUTEUR/2
                    nb_detruits = nb_detruits + 1
                    o_boulet.fini = 1
                    #screen.fill(black) #Bref.
                if ( collision_kamehameha == 1):
                    #Suppression de l'ennemi :
                    screen.blit(jind.noir, jind.rect)
                    liste_ennemis.pop(compteur)
                    nb_detruits = nb_detruits + 1
                    if (etat_son == 1):
                        dora_sound.play()
                        etat_son = 2
                    elif (etat_son == 2):
                        dora2_sound.play()
                        etat_son = 0
                    elif (etat_son == 0):
                        dora3_sound.play()
                        etat_son = 1
                    #screen.fill(black)
                if ( collision_bombe == 1):
                    #Suppression de l'ennemi :
                    screen.blit(jind.noir, jind.rect)
                    liste_ennemis.pop(compteur)
                    nb_detruits = nb_detruits + 1
                    if (etat_son == 1):
                        dora_sound.play()
                        etat_son = 2
                    elif (etat_son == 2):
                        dora2_sound.play()
                        etat_son = 0
                    elif (etat_son == 0):
                        dora3_sound.play()
                        etat_son = 1
                    #screen.fill(black)    
                       
                compteur = compteur + 1
                
                        
                          
            #Gestion rotation tourelle:##################################
            x_mouse,y_mouse = pygame.mouse.get_pos()
            new_tourelle_image = rotation_xy(tourelle_image,x_mouse,y_mouse)
            o_tourelle.rect = new_tourelle_image.get_rect()
            o_tourelle.rect.centerx = LARGEUR/2
            o_tourelle.rect.centery = HAUTEUR/2
            screen.blit(new_tourelle_image, o_tourelle.rect)
            pygame.display.update()
