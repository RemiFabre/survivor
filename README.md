# Survivor
**Let the cringe settle in**
![game_showcase.png](game_showcase.png)

## Install
Tested with Python 3.6 but should work with any 3.XX as long as pygame is installed
```
pip install pygame
```

## Usage
``` 
python survivor.py
```

## Notes
This code is open source but I can't recommend anyone to use it since it's poorly written. 
It was a 2011 christmas holiday project with my brother (he did the art and sounds) when I was just starting to code.