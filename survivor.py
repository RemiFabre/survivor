# -*-coding:Latin-1 -* 
import sys, pygame
import time
import os
from pygame.locals import *
from math import *
from Sources_V18_Beta.AAA_main import *
from Sources_V18_Beta.AAA_classes import *

#from gestion_listes import *

# Un pygame.display.update() me coute entre 13 et 14 ms sur l'asus => fps max = 77


pygame.mixer.init()
pygame.mixer.init()
pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.init()

LARGEUR = 800
HAUTEUR = 600
MAX_SPEED = 1
SPEED_FACTOR = 2
DEMI_DIAGONALE = sqrt(LARGEUR*LARGEUR + HAUTEUR*HAUTEUR)/2 + 40
COMPTEUR = 0
black = 0,0,0
white = 255,255,255
print(DEMI_DIAGONALE)

def afficher_recapitulatif(screen,money,resultat,decalage_x,decalage_y,enable_kamehameha, nb_kamehameha_max,enable_slow_down,nb_slow_down_max,enable_bomb,nb_bomb_max) :
    first_font = pygame.font.SysFont('lucidasans', 20,0,1)#lucidasans
    
    money_string = str(resultat)
    text_surface = first_font.render(money_string, 0, black)
    text_rect = text_surface.get_rect()
    text_rect.left = 420 + decalage_x
    text_rect.centery = 452 + decalage_y
    screen.blit(text_surface, text_rect)

    money_string = str(money)
    text_surface = first_font.render(money_string, 0, black)
    text_rect = text_surface.get_rect()
    text_rect.left = 422 + decalage_x
    text_rect.centery = 484 + decalage_y
    screen.blit(text_surface, text_rect)

    if(enable_kamehameha == 1):
        money_string = str(nb_kamehameha_max)
    else:
        money_string = str(0)    
    text_surface = first_font.render(money_string, 0, black)
    text_rect = text_surface.get_rect()
    text_rect.left = 533 + decalage_x
    text_rect.centery = 514 + decalage_y
    screen.blit(text_surface, text_rect)

    if(enable_slow_down == 1):
        money_string = str(nb_slow_down_max)
    else:
        money_string = str(0)
    text_surface = first_font.render(money_string, 0, black)
    text_rect = text_surface.get_rect()
    text_rect.left = 505 + decalage_x
    text_rect.centery = 545 + decalage_y
    screen.blit(text_surface, text_rect)

    if(enable_bomb == 1):
        money_string = str(nb_bomb_max)
    else:
        money_string = str(0)    
    text_surface = first_font.render(money_string, 0, black)
    text_rect = text_surface.get_rect()
    text_rect.left = 452 + decalage_x
    text_rect.centery = 576 + decalage_y
    screen.blit(text_surface, text_rect)
    
    pygame.display.update()

    
#Debut############################################################################
#Creation de l'ecran et remplissage en noir#######################################
screen = pygame.display.set_mode((LARGEUR, HAUTEUR))
black = 0,0,0
screen.fill(black)

clock = pygame.time.Clock()

start_screen_image = pygame.image.load('Start_screen3.jpg').convert()
start_screen_rect = start_screen_image.get_rect()
credits_image = pygame.image.load('credits2.jpg').convert()
credits_rect = credits_image.get_rect()
shop_image = pygame.image.load('shop_screen2.jpg').convert()
shop_rect = shop_image.get_rect()
croix_image = pygame.image.load('shop_croix.jpg').convert()
croix_rect = croix_image.get_rect()
lvl1_image = pygame.image.load('lvl1.jpg').convert()
lvl1_rect = lvl1_image.get_rect()
lvl2_image = pygame.image.load('lvl2_retouche.jpg').convert()
lvl2_rect = lvl2_image.get_rect()
lvl3_image = pygame.image.load('lvl3_retouche.jpg').convert()
lvl3_rect = lvl3_image.get_rect()

fond_sound = pygame.mixer.Sound('i_can_walk_on_water_remix.wav')
victoire_sound = pygame.mixer.Sound('victoire.wav')

reactualiser = 0
go_credits = 0
go_shop = 0
compteur = 0
compteur2 = 0
black_surface = pygame.Surface((40,40))
black_surface.fill((0, 0, 0))
anti_repetition = 0
money = 3150
money_string = str(money)
enable_kamehameha = 0
nb_kamehameha_max = 0
upgrades_kamehameha = 0
enable_slow_down = 0
nb_slow_down_max = 0
length_slow_down = 0
enable_bomb = 0
nb_bomb_max = 0
text_rect_big = pygame.Rect(161, 78, 60, 20)
text_rect_big.centerx = 161
text_rect_big.centery = 78
a_jour = 0
lvl_reached = 1
somme = 0

#Gestion de la sauvegarde#
if( 1 == os.path.isfile("svd.txt")):
    sauvegarde_existe = 1
    svd = open("./svd.txt","r")
    tout = svd.readlines()
    for i in range(len(tout)):
        tout[i] = tout[i].rstrip('\n')
    print(tout)
    enable_kamehameha = int(tout[0])
    for i in range(5):
        nb_kamehameha_max = nb_kamehameha_max + int(tout[1][i])
    enable_slow_down = int(tout[2])
    for i in range(4):
        nb_slow_down_max = nb_slow_down_max + int(tout[3][i])
    for i in range(2):
        length_slow_down = length_slow_down + int(tout[4][i])    
    enable_bomb = int(tout[5])
    for i in range(2):
        nb_bomb_max = nb_bomb_max + int(tout[6][i])
    money = int(tout[7])
    lvl_reached = int(tout[8])
    somme = somme + enable_kamehameha + nb_kamehameha_max + enable_slow_down + nb_slow_down_max + length_slow_down + enable_bomb + nb_bomb_max + money + lvl_reached
    if( somme != int(tout[9])):
        while(1):
            print("YOU HAVE CHEATED")
    
    svd.close()
else :
    sauvegarde_existe = 0

def sauvegarde():
    if( 1 == os.path.isfile("svd.txt")):
        svd = open("./svd.txt","w")
        if(nb_kamehameha_max == 0):
            str_kame_max = "00000"
        elif(nb_kamehameha_max == 1):
            str_kame_max = "10000"
        elif(nb_kamehameha_max == 2):
            str_kame_max = "11000"
        elif(nb_kamehameha_max == 3):
            str_kame_max = "11100"
        elif(nb_kamehameha_max == 4):
            str_kame_max = "11110"
        elif(nb_kamehameha_max == 5):
            str_kame_max = "11111"
        else:
            print("Probleme avec str_kame_max")
            
        if(nb_slow_down_max == 0):
            str3 = "0000"
        elif(nb_slow_down_max == 1):
            str3 = "1000"
        elif(nb_slow_down_max == 2):
            str3 = "1100"
        elif(nb_slow_down_max == 3):
            str3 = "1110"
        elif(nb_slow_down_max == 4):
            str3 = "1111"
        else:
            print("Probleme avec str3")

        if(length_slow_down == 0):
            str4 = "00"
        elif(length_slow_down == 1):
            str4 = "10"
        elif(length_slow_down == 2):
            str4 = "11"
        else:
            print("Probleme avec str4")

        if(nb_bomb_max == 0):
            str6 = "00"
        elif(nb_bomb_max == 1):
            str6 = "10"
        elif(nb_bomb_max == 2):
            str6 = "11"
        else:
            print("Probleme avec str6")    
            
        str0 = str(enable_kamehameha)
        str1 = str_kame_max
        str2 = str(enable_slow_down) 
        str5 = str(enable_bomb)
        str7 = str(money)
        str8 = str(lvl_reached)
        str9 = str(enable_kamehameha + nb_kamehameha_max + enable_slow_down + nb_slow_down_max + length_slow_down + enable_bomb + nb_bomb_max + money + lvl_reached)
        a_ecrire = "{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}".format(str0,str1,str2,str3,str4,str5,str6,str7,str8,str9)
        svd.write(a_ecrire)
        svd.close()                             
                                     



#Fin gestion sauvegarde

while(1):
    clock.tick(60)
    go_game = 0
    lvl_reached = 4 # A ENLEVER DUDE
    end_ecran_lvl = 0
    resultat = 0
    go_lvl = 0
    
    
    
    if (go_credits != 1 and go_shop !=1):
        screen.blit(start_screen_image, start_screen_rect)
        if (anti_repetition !=1):
            anti_repetition = 1
            sauvegarde()
            pygame.display.update()
        for event in pygame.event.get():
            if event.type == QUIT:
                sauvegarde()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                x_mouse,y_mouse = pygame.mouse.get_pos()
                if (x_mouse > 319 and x_mouse <508 and y_mouse > 390 and y_mouse <477):
                    go_game = 1
                    go_lvl = 1
                if (x_mouse > 336 and x_mouse <496 and y_mouse > 499 and y_mouse <559):    
                    go_credits = 1
                if (x_mouse > 595 and x_mouse <718 and y_mouse > 402 and y_mouse <511):    
                    go_shop = 1
                if (x_mouse > 390 and x_mouse <394 and y_mouse > 129 and y_mouse <134):    
                    fond_sound.play()
                if (x_mouse > 81 and x_mouse <238 and y_mouse > 445 and y_mouse <485):    
                    go_lvl = lvl_reached
                    go_game = 1
                    
    if (go_credits == 1):
        screen.blit(credits_image, credits_rect)
        if (anti_repetition !=2):
            anti_repetition = 2
            pygame.display.update()
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    go_credits = 0
                if (compteur == 0):
                    if event.key == K_UP:
                        compteur = compteur + 1
                if (compteur == 1):
                    if event.key == K_DOWN:
                        compteur = compteur + 1
                if (compteur == 2):
                    if event.key == K_LEFT:
                        compteur = compteur + 1  
                if (compteur == 3):
                    if event.key == K_RIGHT:
                        compteur = compteur + 1   
                if (compteur > 3):
                    compteur = 0
                    go_credits = 0
                    go_game = 1
                    go_lvl = 2
                if (compteur2 == 0):
                    if event.key == K_LEFT:
                        compteur2 = compteur2 + 1
                if (compteur2 == 1):
                    if event.key == K_DOWN:
                        compteur2 = compteur2 + 1
                if (compteur2 == 2):
                    if event.key == K_RIGHT:
                        compteur2 = compteur2 + 1  
                if (compteur2 == 3):
                    if event.key == K_UP:
                        compteur2 = compteur2 + 1   
                if (compteur2 > 3):
                    compteur2 = 0
                    go_credits = 0
                    go_game = 1
                    go_lvl = 3

    if (go_shop == 1 and go_lvl == 0):
        if (anti_repetition !=3):
            screen.blit(shop_image, shop_rect)
            nb_tours = -1
        nb_tours = nb_tours + 1    
        first_font = pygame.font.SysFont('lucidasans', 18,0,1)#lucidasans
        money_string = str(money)
        text_surface = first_font.render(money_string, 0, (100,255,100))
        text_rect = text_surface.get_rect()
        text_rect.centerx = 161
        text_rect.centery = 78
        screen.fill(black, text_rect_big)
        screen.blit(text_surface, text_rect)

        
        if (anti_repetition !=3 or reactualiser != 0):
            anti_repetition = 3
            reactualiser = 0
            pygame.display.update()
    
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    go_shop = 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                x_mouse,y_mouse = pygame.mouse.get_pos()
                if (x_mouse > 602 and x_mouse < 627 and y_mouse > 220 and y_mouse < 242 and money >= 600 and enable_kamehameha == 0):
                    enable_kamehameha = 1
                    croix_rect.left = 602
                    croix_rect.top = 220
                    screen.blit(croix_image , croix_rect)
                    victoire_sound.play()
                    money = money - 600
                    reactualiser = 1
                    pygame.display.update()
                if (x_mouse > 604 and x_mouse < 630 and y_mouse > 269 and y_mouse < 295 and money >= 500 and enable_slow_down == 0):
                    enable_slow_down = 1
                    croix_rect.left = 604
                    croix_rect.top = 269
                    screen.blit(croix_image , croix_rect)
                    victoire_sound.play()
                    money = money - 500
                    reactualiser = 1
                    pygame.display.update()
                if (x_mouse > 603 and x_mouse < 630 and y_mouse > 318 and y_mouse < 344 and money >= 1000 and enable_bomb == 0):
                    enable_bomb = 1
                    croix_rect.left = 603
                    croix_rect.top = 318
                    screen.blit(croix_image , croix_rect)
                    victoire_sound.play()
                    money = money - 1000
                    reactualiser = 1
                    pygame.display.update()
                if (enable_kamehameha == 1):    
                    if (x_mouse > 603 and x_mouse < 630 and y_mouse > 368 and y_mouse < 394 and money >= 500 and nb_kamehameha_max == 0):
                        nb_kamehameha_max = 1
                        croix_rect.left = 603
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 500
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 645 and x_mouse < 670 and y_mouse > 368 and y_mouse < 394 and money >= 500 and nb_kamehameha_max == 1):
                        nb_kamehameha_max = 2
                        croix_rect.left = 645
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 500
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 685 and x_mouse < 711 and y_mouse > 368 and y_mouse < 394 and money >= 500 and nb_kamehameha_max == 2):
                        nb_kamehameha_max = 3
                        croix_rect.left = 685
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 500
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 729 and x_mouse < 755 and y_mouse > 368 and y_mouse < 394 and money >= 500 and nb_kamehameha_max == 3):
                        nb_kamehameha_max = 4
                        croix_rect.left = 729
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 500
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 771 and x_mouse < 798 and y_mouse > 368 and y_mouse < 394 and money >= 500 and nb_kamehameha_max == 4):
                        nb_kamehameha_max = 5
                        croix_rect.left = 771
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 500
                        reactualiser = 1
                        pygame.display.update()
                if (enable_slow_down == 1):    
                    if (x_mouse > 604 and x_mouse < 630 and y_mouse > 418 and y_mouse < 444 and money >= 800 and nb_slow_down_max == 0):
                        nb_slow_down_max = 1
                        croix_rect.left = 604
                        croix_rect.top = 418
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 800
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 647 and x_mouse < 673 and y_mouse > 418 and y_mouse < 444 and money >= 800 and nb_slow_down_max == 1):
                        nb_slow_down_max = 2
                        croix_rect.left = 647
                        croix_rect.top = 418
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 800
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 687 and x_mouse < 713 and y_mouse > 418 and y_mouse < 444 and money >= 800 and nb_slow_down_max == 2):
                        nb_slow_down_max = 3
                        croix_rect.left = 687
                        croix_rect.top = 418
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 800
                        reactualiser = 1
                        pygame.display.update()
                if (enable_slow_down == 1):    
                    if (x_mouse > 604 and x_mouse < 630 and y_mouse > 468 and y_mouse < 494 and money >= 1000 and length_slow_down == 0):
                        length_slow_down = 1
                        croix_rect.left = 604
                        croix_rect.top = 468
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 1000
                        reactualiser = 1
                        pygame.display.update()
                if (enable_bomb == 1):    
                    if (x_mouse > 604 and x_mouse < 630 and y_mouse > 521 and y_mouse < 546 and money >= 1000 and nb_bomb_max == 0):
                        nb_bomb_max = 1
                        croix_rect.left = 604
                        croix_rect.top = 521
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 1000
                        reactualiser = 1
                        pygame.display.update()
                    if (x_mouse > 645 and x_mouse < 671 and y_mouse > 520 and y_mouse < 546 and money >= 1000 and nb_bomb_max == 1):
                        nb_bomb_max = 2
                        croix_rect.left = 645
                        croix_rect.top = 520
                        screen.blit(croix_image , croix_rect)
                        victoire_sound.play()
                        money = money - 1000
                        reactualiser = 1
                        pygame.display.update()     
                    
        if (enable_kamehameha == 1 and nb_tours < 1):
            croix_rect.left = 602
            croix_rect.top = 220
            screen.blit(croix_image , croix_rect)  
            reactualiser = 1
        if (nb_kamehameha_max >= 1 and nb_tours < 1):
            croix_rect.left = 603
            croix_rect.top = 368
            screen.blit(croix_image , croix_rect)
            if (nb_kamehameha_max >= 1 and nb_tours < 1):
                croix_rect.left = 645
                croix_rect.top = 368
                screen.blit(croix_image , croix_rect)
                if (nb_kamehameha_max >= 1 and nb_tours < 1):
                    croix_rect.left = 685
                    croix_rect.top = 368
                    screen.blit(croix_image , croix_rect)
                    if (nb_kamehameha_max >= 1 and nb_tours < 1):
                        croix_rect.left = 729
                        croix_rect.top = 368
                        screen.blit(croix_image , croix_rect)
                        if (nb_kamehameha_max >= 1 and nb_tours < 1):
                            croix_rect.left = 771
                            croix_rect.top = 368
                            screen.blit(croix_image , croix_rect)
                            
        if (nb_slow_down_max >= 1 and nb_tours < 1):
            croix_rect.left = 604
            croix_rect.top = 418
            screen.blit(croix_image , croix_rect)
            if (nb_slow_down_max >= 2 and nb_tours < 1):
                croix_rect.left = 647
                croix_rect.top = 418
                screen.blit(croix_image , croix_rect)
                if (nb_slow_down_max >= 3 and nb_tours < 1):
                    croix_rect.left = 687
                    croix_rect.top = 418
                    screen.blit(croix_image , croix_rect)

        if (nb_bomb_max >= 1 and nb_tours < 1):
            croix_rect.left = 604
            croix_rect.top = 521
            screen.blit(croix_image , croix_rect)
            if (nb_slow_down_max >= 2 and nb_tours < 1):
                croix_rect.left = 645
                croix_rect.top = 520
                screen.blit(croix_image , croix_rect)           
                    
        if (length_slow_down >= 1 and nb_tours < 1):
            croix_rect.left = 604
            croix_rect.top = 468
            screen.blit(croix_image , croix_rect)            
                    
        if (enable_slow_down == 1 and nb_tours < 1):
            croix_rect.left = 604
            croix_rect.top = 269
            screen.blit(croix_image , croix_rect)
            reactualiser = 1
        if (enable_bomb == 1 and nb_tours < 1):
            croix_rect.left = 603
            croix_rect.top = 318
            screen.blit(croix_image , croix_rect)
            reactualiser = 1    
                                        
    if (go_game == 1):
        fond_sound.stop()
        anti_repetition = 0
        compteur = 0
        compteur2 = 0
        #screen.fill(black)
        #main(screen,0)#Pour test
        if (go_lvl == 1):
            screen.fill(black)
            screen.blit(lvl1_image, lvl1_rect)
            afficher_recapitulatif(screen,money,0,0,0,enable_kamehameha,nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,enable_bomb,1+nb_bomb_max)
            while(1):
                if (end_ecran_lvl == 1) :
                    end_ecran_lvl = 0
                    break
                for event in pygame.event.get():
                    if (event.type == KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN):
                        end_ecran_lvl = 1
                        break
            resultat = main(screen, 1, enable_kamehameha, nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,length_slow_down,enable_bomb,1+nb_bomb_max)        
            if (resultat == -1):
                continue
            else:
                money = money + resultat 
                print("Pour acceder au niveau 2 directement: Allez dans Credits puis faites HAUT, BAS, GAUCHE, DROITE")
                if (lvl_reached < 2):
                    lvl_reached = 2
                go_lvl = 2
        if (go_lvl == 2):
            screen.fill(black)
            screen.blit(lvl2_image, lvl2_rect)
            afficher_recapitulatif(screen,money,resultat,0,0,enable_kamehameha,nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,enable_bomb,1+nb_bomb_max)
            while(1):
                if (end_ecran_lvl == 1) :
                    end_ecran_lvl = 0
                    break
                for event in pygame.event.get():
                    if (event.type == KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN):
                        end_ecran_lvl = 1
                        break
            resultat = main(screen, 2, enable_kamehameha, nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,length_slow_down,enable_bomb,1+nb_bomb_max)        
            if (resultat == -1):
                continue
            else:
                money = money + resultat
                print("Pour acceder au niveau 3 directement: Allez dans Credits puis faites GAUCHE,BAS, DROITE,HAUT")
                if (lvl_reached < 3):
                    lvl_reached = 3
                go_lvl = 3
        if (go_lvl == 3):    
            screen.fill(black)
            screen.blit(lvl3_image, lvl3_rect)
            afficher_recapitulatif(screen,money,resultat,-220,8,enable_kamehameha,nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,enable_bomb,1+nb_bomb_max)
            while(1):
                if (end_ecran_lvl == 1) :
                    end_ecran_lvl = 0
                    break
                for event in pygame.event.get():
                    if (event.type == KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN):
                        end_ecran_lvl = 1
                        break
            resultat = main(screen, 3, enable_kamehameha, nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,length_slow_down,enable_bomb,1+nb_bomb_max)        
            if (resultat == -1):
                continue
            else:
                money = money + resultat
                lvl_reached = 4
                go_lvl = go_lvl + 1

        while(1):
            if (go_lvl > 3):    
                screen.fill(black)
                screen.blit(lvl1_image, lvl1_rect)
                afficher_recapitulatif(screen,money,resultat,0,0,enable_kamehameha,nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,enable_bomb,1+nb_bomb_max)
                while(1):
                    if (end_ecran_lvl == 1) :
                        end_ecran_lvl = 0
                        break
                    for event in pygame.event.get():
                        if (event.type == KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN):
                            end_ecran_lvl = 1
                            break
                resultat = main(screen, go_lvl, enable_kamehameha, nb_kamehameha_max*2+3,enable_slow_down,1+nb_slow_down_max,length_slow_down,enable_bomb,1+nb_bomb_max)        
                if (resultat == -1):
                    break
                else:
                    money = money + resultat
                    lvl_reached = lvl_reached + 1
                    go_lvl = go_lvl + 1
            
